let http = require("http");
http.createServer(function(request, response){


	//1. homepage GET route

	if(request.url == "/" && request.method == "GET"){

		response.writeHead(200, {'Content-Type':'text/plain'});
		response.end('Welcome to the booking system');
	}


	//2.profile GET route

	else if(request.url == "/profile" && request.method == "GET"){

		response.writeHead(200, {'Content-Type':'text/plain'});
		response.end('Welcome to your profile');
	}


	//3.courses GET route

	else if(request.url == "/courses" && request.method == "GET"){

		response.writeHead(200, {'Content-Type':'text/plain'});
		response.end("Here's our courses available");
	}


	//4.courses POST route

	else if(request.url == "/courses" && request.method == "POST"){

		response.writeHead(200, {'Content-Type':'text/plain'});
		response.end("Add course to our resources");
	}


	//5.courses PUT route

	else if(request.url == "/courses" && request.method == "PUT"){

		response.writeHead(200, {'Content-Type':'text/plain'});
		response.end("Update a course to our resources");
	}


	//6.courses DELETE route

	else if(request.url == "/courses" && request.method == "DELETE"){

		response.writeHead(200, {'Content-Type':'text/plain'});
		response.end("Archive courses to our resources");
	}

	}).listen(4000);
	console.log('Server running at localhost:4000');