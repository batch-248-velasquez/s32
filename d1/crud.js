let http = require ("http");

// Mock Database

let directory = [
		{
			"name": "Ash Ketchum",
			"email": "besttrainer@mail.com"

		},
		{
			"name": "Jose Masipag",
			"email": "bawalangtamad@mail.com"

		},
		{
			"name": "Hanamichi Sakuragi",
			"email": "reboundislife@mail.com"

		}
	]


http.createServer(function(request,response){

	if(request.url == "/items" && request.method == "GET"){

		
		response.writeHead(200, {'Content-Type':'text/plain'});
	
		response.end('This is a response to a GET method request for the / endpoint.');
	}

	else if (request.url == "/items" && request.method == "POST"){
		
		response.writeHead(200, {'Content-Type':'text/plain'});
		response.end('This is a response to a POST method request for the / endpoint');
	}

	else if (request.url == "/items" && request.method == "PUT"){
		
		response.writeHead(200, {'Content-Type':'text/plain'});
		response.end('This is a response to a PUT method request for the / endpoint.');
	}

	else if (request.url == "/items" && request.method == "DELETE"){
		
		response.writeHead(200, {'Content-Type':'text/plain'});
		response.end('This is a response to a DELETE method request for the / endpoint.');
	}

	//Get Users

	//Route for returnining all users upon receiving out GET request

	else if (request.url == "/users" && request.method == "GET"){

		response.writeHead(200,{'Content-Type':'application/json'})

		response.write(JSON.stringify(directory));
		response.end();
	}

	//Route to add a new user
		//we have to receive an input from the client

	else if (request.url == "/users" && request.method == "POST"){

		let requestBody = '';

		request.on('data', function(userData){
			
			requestBody += userData;
			
			console.log(userData);

		})

			request.on('end', function(){
				requestBody = JSON.parse(requestBody);

				console.log(requestBody);
				directory.push(requestBody);

				console.log(directory);

				response.writeHead(200,{'Content-Type':'application/json'});
				response.end(JSON.stringify(directory))


			})

}

}).listen(4000);

console.log ('Server running at localhost:4000 (crud.js)')